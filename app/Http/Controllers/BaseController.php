<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class BaseController extends Controller
{
    public function execWithJsonResponse($callback)
    {
        try{
            DB::beginTransaction();
            $response= $callback();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            report($exception);
            $response= response()->json(['errors'=> [
                '0'=> [$exception->getMessage()]
            ]],  422);
        }catch (\Throwable $exception){
            DB::rollBack();
            report($exception);
            $response= response()->json(['errors'=> [
                '0'=> [$exception->getMessage()]
            ]],  422);
        }
        return $response;
    }

    public function execWithJsonSuccessResponse($callback)
    {
        try{
            DB::beginTransaction();
            $response= $callback();
            $response= array_merge([
                'success'=> true,
                'code'=> 200,
                'message'=> ''
            ], $response);

            DB::commit();
        }catch (ValidationException $exception){
            DB::rollBack();

            $errors= [];
            foreach ($exception->errors() as $field=> $error) {
                $errors[] = "$field: ".join(', ', $error).'.';
            }
            $response= response()->json([
                'success'=> false,
                'code'=> 422,
                'message'=> join( ' ', $errors)
            ],  422);

        }catch (\Exception $exception){
            DB::rollBack();
            report($exception);
            $code= $exception->getCode()<100 || $exception->getCode()>599?500:$exception->getCode();
            $code= (int) $code;
            $response= response()->json([
                'success'=> false,
                'code'=> $exception->getCode(),
                'message'=> $exception->getMessage()
            ], $code<100 || $code>599?500:$code);
        }catch (\Throwable $exception){
            DB::rollBack();
            report($exception);
            $code= $exception->getCode()<100 || $exception->getCode()>599?500:$exception->getCode();
            $code= (int) $code;
            $response= response()->json([
                'success'=> false,
                'code'=> $exception->getCode(),
                'message'=> $exception->getMessage()
            ], $code<100 || $code>599?500:$code);
        }
        return $response;
    }

}

