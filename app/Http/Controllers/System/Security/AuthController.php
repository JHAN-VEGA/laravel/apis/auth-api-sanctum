<?php

namespace App\Http\Controllers\System\Security;

use App\Http\Controllers\BaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AuthController extends BaseController
{
    public function __construct()
    {
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'exists:users,email'],
            'password' => ['required', 'string']
        ]);

        return $this->execWithJsonSuccessResponse(function () use ($request){
            if (!Auth::attempt($request->only(['email', 'password']))) {
                return [
                    'success' => false,
                    'message' => 'Unauthorized',
                    'code' => 401
                ];
            }

            auth()->user()->tokens()->forceDelete();

            $token = auth()->user()
                ->createToken('auth_token')
                ->plainTextToken;

            /* $roles = auth()->user()->currentAccessToken()->roles;
            $permissions = auth()->user()->currentAccessToken()->permissions; */

            auth()->user()->syncRoles(Role::all());
            auth()->user()->syncPermissions(Permission::all());

            return [
                '_token' => $token,
                'message' => 'Loggin success',
                'roles' => auth()->user()->getRoleNames(),
                'permissions' => auth()->user()->getPermissionNames()
            ];
        });

    }

    public function test()
    {
        return $this->execWithJsonSuccessResponse(function () {

            return [
                'products' => [
                    [
                        'id' => 1,
                        'name' => 'test 1',
                        'description' => 'test description 1',
                    ],
                    [
                        'id' => 2,
                        'name' => 'test 2',
                        'description' => 'test description 2',
                    ],
                    [
                        'id' => 3,
                        'name' => 'test 3',
                        'description' => 'test description 3',
                    ],
                ]
            ];
        });
    }

}
